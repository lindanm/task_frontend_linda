//Navbar smooth scrolling javascript
$('.navbar a').on('click', function(e) {
    if(this.hash !== '') {
        e.preventDefault();

        const hash = this.hash;

        $('html, body').animate(
            {
                scrollTop: $(hash).offset().top
            },
            800
        );
    }
});

//navbar active yellow
$(".navbar-light li").on("click", function() {
    $(".navbar-light li").removeClass("active");
    $(this).addClass("active");
});

//setting display carousel responsive
$('.owl-carousel').owlCarousel({
    // loop: true,
    margin: 80,
    // nav: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});